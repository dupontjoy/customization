###個人維護的一些規則

by Cing

| | |
| :--- | :--- |
| **NoScript-Whitelist.txt** | Noscript白名單 |
| **ucjsPermission-Blacklist.txt** | [ucjsPermission2][1]腳本 要阻止的黑名單 |
| **ucjsPermission-Whitelist.txt** | ucjsPermission2腳本 要放行的白名單 |

  [1]: https://github.com/dupontjoy/userChrome.js-Collections-/tree/master/ucjsPermission2.uc.xul
