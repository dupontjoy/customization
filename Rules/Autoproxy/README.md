###個人維護的一些規則

by Cing

| | |
| :--- | :--- |
| **Autoproxy-self-rules.txt** | 自定义的翻墙网站列表 |
| **gfwlist.txt** | [官方规則][1]搬运 |
| **gfw.bricks** | [sites blocked by GFW][2]搬运 |

  [1]: https://autoproxy-gfwlist.googlecode.com/svn/trunk/gfwlist.txt
  [2]: https://github.com/Leask/BRICKS/blob/master/gfw.bricks
