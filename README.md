| | |
| :--- | :--- |
| 12306 | 重定向12306的js到修改版，用法：[Redirector][1] |
| Foxmail-Filter | Foxmail過濾器 |
| netease | 网易云音乐js（已失效） |
| **ProcessLasso** | ProcessLasso進程管理軟件設置 |
| **QTTabBar** | QTTabBar中文简体语言文件等 |
| Rules | 一些自己维护的規則 |
| zzz-NotNow | 暂时不用的脚本 |
| persdict.dat | 自定义的词典 |
| **stylish.sqlite** | Stylish样式文件 |
| **user.js** | Firefox用戶參數，常用參數導出，用以覆蓋prefs.js |
| | [Firefox Profilemaker](https://ffprofile.com/#start)：在線定製prefs.js參數 |

[1]: https://github.com/dupontjoy/userChrome.js-Collections-/tree/master/Redirector